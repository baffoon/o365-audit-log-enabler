# O365 Audit Log Enabler

Enables full audit logging of supported Office 365 - Exchange Online tenants.

### Notice:

I am not responsible for any damage that may result in running this script.

### Script Usage

1. Obtain the credentials for the Office 365 tennant's Administrator account.
2. Run the script in it's directory ```./Enable-O365AuditLogs.ps1```
3. Enter said Office 365 Credentials.
4. Let it run.
5. Review the outputted report.
6. Troubleshoot as needed.

### Known Errors:

Please note that there is no error checking or exception handling. So you may recieve errors or warnings:

#### Operation is not required:

This is recieved when enabling Organization Customization. If this were disabled, you wouldn't be able to edit the attributes needed
to run the script:


```PowerShell
This operation is not required. Organization is already enabled for customization.
....
```


#### Execution Policy Error

This error comes from running scripts when PowerShell execution policy is set to restricted:


```PowerShell
Enable-O365AuditLogs.ps1 cannot be loaded because running scripts is disabled on this system.
....
```


You can resolve this by setting your exectuion policy to unrestricted.





