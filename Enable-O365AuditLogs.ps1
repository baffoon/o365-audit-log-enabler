# Office 365 Audit Log Enabler
# Script will enable audit logs for Office 365
Write-Host -Object "Office 365 Audit Log Enabler"
Write-Host -Object "[*] Obtaining credentials from user..."
$O365Credentials = Get-Credential

Write-Host -Object "[*] Creating new powershell Session..."
$Session = New-PSSession -ConfigurationName Microsoft.Exchange `
                         -ConnectionUri https://outlook.office365.com/powershell-liveid/ `
                         -Credential $O365Credentials `
                         -Authentication Basic `
                         -AllowRedirection

Write-Host -Object "[*] Importing PowerShell session for Office 365 tenent..."
Import-PSSession -Session $Session

Write-Host -Object "[*] Enabling organization customization for the tenent..."
Enable-OrganizationCustomization

Write-Host -Object "[*] Setting AdminAuditLogConfig to true..."
Set-AdminAuditLogConfig -UnifiedAuditLogIngestionEnabled $true

Write-Host -Object "[*] Enabling Auditing for all users..."
Write-Host -Object "[*] Setting audit age limit to 90 days..."
Write-Host -Object "[*] Configuring AuditDelegate rules..."
Write-Host -Object "[*] Configuring AuditAdmin rules..."
Write-Host -Object "[*] Configuring AuditOwner rules..."
Get-Mailbox -ResultSize Unlimited -Filter {RecipientTypeDetails -eq "UserMailbox"} | ForEach { Set-Mailbox $_.SAMAccountName -AuditEnabled $true `
                                                                                                                             -AuditLogAgeLimit 90.00:00:00 `
                                                                                                                             -AuditDelegate @{Add="Create","FolderBind","SendAs","SendOnBehalf","SoftDelete","HardDelete","Update","Move","MoveToDeletedItems","UpdateFolderPermissions"} `
                                                                                                                             -AuditAdmin @{Add="Create","FolderBind","MessageBind","SendAs","SendOnBehalf","SoftDelete","HardDelete","Update","Move","Copy","MoveToDeletedItems","UpdateFolderPermissions"} `
                                                                                                                             -AuditOwner @{Add="Create","HardDelete","Move","MoveToDeletedItems","MailboxLogin","SoftDelete","Update","UpdateFolderPermissions","UpdateCalendarDelegation","UpdateInboxRules"}}

Write-Host -Object "[*] Configuration complete...Working on generating report..."
Get-AdminAuditLogConfig | Format-List -Property UnifiedAuditLogIngestionEnabled
Get-Mailbox | Format-List -Property Name,EmailAddress,Audit*,MessageTracking

Write-Host -Object "[*] Cleaning up PowerShell Sessions..."
Get-PSSession | Remove-PSSession
